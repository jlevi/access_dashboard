<?php

session_start();

// check if the user has requested to clear the PHP session - useful
// for login errors

if (isset($_GET['reset_Session']) && $_GET['reset_session'] == true) {
    $_SESSION = [];
    session_unset();
    session_destroy();
    session_start();
}

// start timing the session
$time_start = microtime(true);

// define output format
$output_format = 'json';

// require config file
require_once __DIR__.'/config.php';

// require UniFi API Class
require_once __DIR__.'/class.unifi.php';

// determine if we have reached the cookie timeout, if so, refresh the PHP session
// els,, update last activity time stamp
if(isset($_SESSION['last_activity']) && (time() - $_SESSION['last_activity'] > $cookietimeout)) {
    // last activity was longer than $cookietimeout seconds ago
    session_unset();
    session_destroy();
    if($debug) {
        error_log('session cookie timeout');
    }
}

$_SESSION['last_activity'] = time();

// collect curl info
$curl_info = curl_version();
$curl_version = $curl_info['version'];


// site id we are querying
$site_id = 'gxle396h';

// get requested action from user or action stored in $_SESSION array
if (isset($_GET['action'])) {
    $action             = $_GET['action'];
    $_SESSION['action'] = $action; 
} else {
    if (isset($_SESSION['action'])) {
        $action = $_SESSION['action'];
    }
}

// define session controller to use
$_SESSION['controller'] = [
    'user'      => $controlleruser,
    'password'  => $controllerpassword,
    'url'       => $controllerurl,
    'name'      => 'Controller',
    'version'   => $controllerversion
];
$controller = $_SESSION['controller'];

// get action from AJAX call
if (isset($_GET['action'])) {
    if($_GET['action'] == 'login'){

        // make sure controller has been set/selected
        if (isset($_SESSION['controller'])){

            // create new instance of unifi api and log into unifi controller
            $unifidata      = new UnifiApi($controller['user'], $controller['password'], $controller['url'], $site_id, $controller['version']);
            $set_debug_mode = $unifidata->set_debug(trim($debug));
            $loginresults   = $unifidata->login();

            if($loginresults === 400) {
                echo 'Error logging in. Please check your credentials and try again';
            } else {
                header('Content-Type: application/json');
                echo json_encode($loginresults, JSON_PRETTY_PRINT);
            }
        }   
    }

    // get main AP devices list
    if($_GET['action'] == 'list_devices') {

        $unifidata      = new UnifiApi($controller['user'], $controller['password'], $controller['url'], $site_id, $controller['version']);
        $set_debug_mode = $unifidata->set_debug(trim($debug));
        $loginresults   = $unifidata->login();

        $listDevices      = $unifidata->list_devices();

        /**
        * output the results in HTML format
        */
        header('Content-Type: application/json');
        print_output('json', $listDevices);

    }

}

/**
 * shared functions
 */
function print_output($output_format, $data)
{
    /**
     * function to print the output
     * switch depending on the selected $output_format
     */
    switch ($output_format) {
        case 'json':
            echo json_encode($data, JSON_PRETTY_PRINT);
            break;
        case 'json_color':
            echo json_encode($data);
            break;
        case 'php_array':
            print_r($data);
            break;
        case 'php_var_dump':
            var_dump($data);
            break;
        case 'php_var_export':
            var_export($data);
            break;
        default:
            echo json_encode($data, JSON_PRETTY_PRINT);
            break;
    }
}

?>