(function($) {

    'use strict';

    $(document).ready(function(){
        var loginForm  = document.getElementById('adminLogin');

        // $(function() {
        //     $('#adminLogin').submit(function(e) {
        //         e.preventDefault();
        //         location.href = service.BASE_URL + 'dashboard.html';
        //         // location.href = service.DEV_URL + 'dashboard.php';
        //         // location.href = service.API_URL + 'dashboard.php';
        //     });
        // });

        // validate login form
        $(function() {
            $(loginForm).validate({
                rules: {
                    username:{
                        required: true
                    },
                    password: {
                        required: true,
                        minlength: 6
                    }
                },
                messages: {
                    username: {
                        required: "Please enter a valid username to continue"
                    },
                    password: {
                        required: "A password is needed in order to proceed",
                        minlength: "Your password should be at least 6 characters long"
                    }
                },
                submitHandler: loginSubmit
            });
        });


        // perform actual login
        function loginSubmit(){
            // $('#login-btn').html('');
            $('#login-btn').addClass('spinner');
            var data  = $(loginForm).serialize();
            
            $.ajax({
                type: 'POST',
                url: 'php/process_login.php',
                data: data,
                success: function(response){
                    var res = JSON.parse(response);
                    if (res.message == 'Success') {
                        $('#success_msg').removeClass('hide');
                        $('#login-btn').removeClass('spinner');
                        setTimeout(' window.location.href = "dashboard.php"; ',4000);
                    } else {
                        $('#error_msg').removeClass('hide');
                        console.log('wrong username/password. please try again...');
                    }
                    // console.log(res.message);

                }
            });
        }
        
    });
}(jQuery));