(function ($) {

    'use strict';

    $(document).ready(function () {

        // var listClients = <?php echo json_encode($listClients); ?>;
        // console.log(listClients);

        $('.loader').fadeOut('slow');

        // render pagination
        $('#client_collapse').simplePagination({
            perPage:2,
            currentPage: 1
        })

        $('#getDevices').click(function (e) {
            e.preventDefault();

            $.ajax({
                method: 'GET',
                url: 'php/dashboard.php',
                data: 'action=list_devices',
                complete: function (res) {
                    console.log('responded...');
                    console.log(res);
                },
                error: function () {
                    console.log('error processing request...');
                }
            });
            return false;
        });

        function renderUsageGraph() {
            var data = {
                labels: ["6AM", "7AM", "8AM", "9AM", "10AM", "11AM", "12PM", "1PM", "2PM", "3PM", "4PM", "5PM"],
                data: [2, 4.5, 3, 4, 3, 5.6, 3, 6.6, 2, 5, 6, 1]
            };

            var options = {
                smooth: false,
                stroke: {
                    width: 1
                },
                points: {
                    fill: true,
                    radius: 3,
                    strokeWidth: 0
                },
                areaOpacity: 0.05
            };

            var usgline = new Grafs.Line('#usage-chart', data, options);

            // $('.grafs-grid-left-y-labels .grafs-label').append('GB');
        }


        // call above functions
        // dashboardInit();
        // apDevices();
        renderUsageGraph();
    });
}(jQuery));