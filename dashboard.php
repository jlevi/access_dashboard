<?php
/**
 * UniFi API browser
 *
 * This tool is for browsing data that is exposed through Ubiquiti's UniFi Controller API,
 * written in PHP, JavaScript and the Bootstrap CSS framework.
 *
 * Please keep the following in mind:
 * - not all data collections/API endpoints are supported (yet), see the list of
 *   the currently supported data collections/API endpoints in the README.md file
 * - this tool currently supports versions 4.x and 5.x of the UniFi Controller software
 *
 * VERSION: 1.0.17
 *
 * ------------------------------------------------------------------------------------
 *
 * Copyright (c) 2016, Slooffmaster
 *
 * This source file is subject to the MIT license that is bundled
 * with this package in the file LICENSE.md
 *
 */
define('API_BROWSER_VERSION', '1.0.17');

/**
 * in order to use the PHP $_SESSION array for temporary storage of variables, session_start() is required
 */
session_start();

/**
 * check whether user has requested to clear the PHP session
 * - this function can be useful when login errors occur, mostly after upgrades or incorrect credential changes
 */
if (isset($_GET['reset_session']) && $_GET['reset_session'] == true) {
    $_SESSION = [];
    session_unset();
    session_destroy();
    session_start();
}

/**
 * starting timing of the session here
 */
$time_start = microtime(true);

/**
 * assign variables which are required later on together with their default values
 */
$controller_id = '';
$action        = '';
$site_id       = 'gxle396h';
$site_name     = '';
$selection     = '';
$output_format = 'json';
$data          = '';
$objects_count = '';
$alert_message = '';

/**
 * load the configuration file
 * - allows override of several of the previously declared variables
 * - if the config.php file is unreadable or does not exist, an alert is displayed on the page
 */
if(!is_readable('config.php')) {
    $alert_message = '<div class="alert alert-danger" role="alert">The file <code>config.php</code> is not readable or does not exist.'
                   . '<br>If you have not yet done so, please copy/rename the <code>config.template.php</code> file to <code>config.php</code> and follow '
                   . 'the instructions inside to enter your credentials and controller details.</div>';
} else {
    // require_once('config.php');
    require_once __DIR__.'/php/config.php';
}

/**
 * load the UniFi API client class
 */
// require_once('phpapi/class.unifi.php');
require_once __DIR__.'/php/class.unifi.php';

/**
 * determine whether we have reached the cookie timeout, if so, refresh the PHP session
 * else, update last activity time stamp
 */
if (isset($_SESSION['last_activity']) && (time() - $_SESSION['last_activity'] > $cookietimeout)) {
    /**
     * last activity was longer than $cookietimeout seconds ago
     */
    session_unset();
    session_destroy();
    if ($debug) {
        error_log('UniFi API browser INFO: session cookie timed out');
    }
}

$_SESSION['last_activity'] = time();

/**
 * collect cURL version details for the info modal
 */
$curl_info    = curl_version();
$curl_version = $curl_info['version'];

/**
 * process the GET variables and store them in the $_SESSION array
 * if a GET variable is not set, get the values from the $_SESSION array (if available)
 *
 * Process in this order:
 * - controller_id
 * Only process this after controller_id is set:
 * - site_id
 * Only process these after site_id is set:
 * - action
 * - output_format
 * - theme
 */
if (isset($_GET['controller_id'])) {
    /**
     * user has requested a controller switch
     */
    $controller                = $controllers[$_GET['controller_id']];
    $controller_id             = $_GET['controller_id'];
    $_SESSION['controller']    = $controller;
    $_SESSION['controller_id'] = $_GET['controller_id'];

    /**
     * clear the variables from the $_SESSION array that are associated with the previous controller
     */
    unset($_SESSION['site_id']);
    unset($_SESSION['site_name']);
    unset($_SESSION['sites']);
    unset($_SESSION['action']);
    unset($_SESSION['detected_controller_version']);
} else {
    if (isset($_SESSION['controller']) && isset($controllers)) {
        $controller    = $_SESSION['controller'];
        $controller_id = $_SESSION['controller_id'];
    } else {
        if (!isset($controllers)) {
            /**
             * if the user has configured a single controller, we push it's details
             * to the $_SESSION and $controller arrays
             */
            $_SESSION['controller'] = [
                'user'     => 'einheit',
                'password' => 'passIT@3402',
                'url'      => 'https://207.154.207.79:8443',
                'name'     => 'Controller',
                'version'  => '5.4.16'
            ];
            $controller = $_SESSION['controller'];
        }
    }

    if (isset($_GET['site_id'])) {
        $site_id               = $_GET['site_id'];
        $_SESSION['site_id']   = $site_id;
        $site_name             = $_GET['site_name'];
        $_SESSION['site_name'] = $site_name;
    } else {
        if (isset($_SESSION['site_id'])) {
            $site_id   = $_SESSION['site_id'];
            $site_name = $_SESSION['site_name'];
        }
    }
}

/**
 * get requested theme or use the theme stored in the $_SESSION array
 */
if (isset($_GET['theme'])) {
    $theme             = $_GET['theme'];
    $_SESSION['theme'] = $theme;
} else {
    if (isset($_SESSION['theme'])) {
        $theme = $_SESSION['theme'];
    }
}

/**
 * get requested output_format or use the output_format stored in the $_SESSION array
 */
if (isset($_GET['output_format'])) {
    $output_format             = $_GET['output_format'];
    $_SESSION['output_format'] = $output_format;
} else {
    if (isset($_SESSION['output_format'])) {
        $output_format = $_SESSION['output_format'];
    }
}

/**
 * get requested action or use the action stored in the $_SESSION array
 */
if (isset($_GET['action'])) {
    $action             = $_GET['action'];
    $_SESSION['action'] = $action;
} else {
    if (isset($_SESSION['action'])) {
        $action = $_SESSION['action'];
    }
}

/**
 * display info message when no controller, site or data collection is selected
 * placed here so they can be overwritten by more "severe" error messages later on
 */
if ($action === '') {
    $alert_message = '<div class="alert alert-info" role="alert">Please select a data collection/API endpoint from the dropdown menus'
                   . ' <i class="fa fa-arrow-circle-up"></i></div>';
}

if ($site_id === '' && isset($_SESSION['controller'])) {
    $alert_message = '<div class="alert alert-info" role="alert">Please select a site from the Sites dropdown menu <i class="fa fa-arrow-circle-up">'
                   . '</i></div>';
}

if (!isset($_SESSION['controller'])) {
    $alert_message = '<div class="alert alert-info" role="alert">Please select a controller from the Controllers dropdown menu <i class="fa fa-arrow-circle-up">'
                   . '</i></div>';
}

/**
 * Do this when a controller has been selected and was stored in the $_SESSION array
 */
if (isset($_SESSION['controller'])) {
    /**
     * create a new instance of the API client class and log in to the UniFi controller
     * - if an error occurs during the login process, an alert is displayed on the page
     */
    $unifidata      = new UnifiApi($controller['user'], $controller['password'], $controller['url'], $site_id, $controller['version']);
    $set_debug_mode = $unifidata->set_debug(trim($debug));
    $loginresults   = $unifidata->login();

    if($loginresults === 400) {
        $alert_message = '<div class="alert alert-danger" role="alert">HTTP response status: 400'
                       . '<br>This is probably caused by a UniFi controller login failure, please check your credentials in '
                       . 'config.php. After correcting your credentials, please restart your browser or use the <b>Reset PHP session</b> function in the dropdown '
                       . 'menu on the right, before attempting to use the API browser tool again.</div>';
    } 

    /**
     * Get the list of sites managed by the UniFi controller (if not already stored in the $_SESSION array)
     */
    if (!isset($_SESSION['sites']) || $_SESSION['sites'] === '') {
        $sites  = $unifidata->list_sites();
        $_SESSION['sites'] = $sites;
    } else {
        $sites = $_SESSION['sites'];
    }

    /**
     * Get the version of the UniFi controller (if not already stored in the $_SESSION array or when 'undetected')
     */
    if (!isset($_SESSION['detected_controller_version']) || $_SESSION['detected_controller_version'] === 'undetected') {
        $site_info = $unifidata->stat_sysinfo();

        if (isset($site_info[0]->version)) {
            $detected_controller_version             = $site_info[0]->version;
            $_SESSION['detected_controller_version'] = $detected_controller_version;
        } else {
            $detected_controller_version             = 'undetected';
            $_SESSION['detected_controller_version'] = 'undetected';
        }

    } else {
        $detected_controller_version = $_SESSION['detected_controller_version'];
    }
}

/**
 * execute timing of controller login
 */
$time_1           = microtime(true);
$time_after_login = $time_1 - $time_start;

if (isset($unifidata)) {
    /**
     * select the required call to the UniFi Controller API based on the selected action
     */
    switch ($action) {
        case 'list_clients':
            $selection = 'list online clients';
            $data      = $unifidata->list_clients();
            break;
        case 'stat_allusers':
            $selection = 'stat all users';
            $data      = $unifidata->stat_allusers();
            break;
        case 'stat_auths':
            $selection = 'stat active authorisations';
            $data      = $unifidata->stat_auths();
            break;
        case 'list_guests':
            $selection = 'list guests';
            $data      = $unifidata->list_guests();
            break;
        case 'list_usergroups':
            $selection = 'list usergroups';
            $data      = $unifidata->list_usergroups();
            break;
        case 'stat_hourly_site':
            $selection = 'hourly site stats';
            $data      = $unifidata->stat_hourly_site();
            break;
        case 'stat_daily_site':
            $selection = 'daily site stats';
            $data      = $unifidata->stat_daily_site();
            break;
        case 'stat_hourly_aps':
            $selection = 'hourly ap stats';
            $data      = $unifidata->stat_hourly_aps();
            break;
        case 'stat_daily_aps':
            $selection = 'daily ap stats';
            $data      = $unifidata->stat_daily_aps();
            break;
        case 'stat_sysinfo':
            $selection = 'sysinfo';
            $data      = $unifidata->stat_sysinfo();
            break;
        case 'list_devices':
            $selection = 'list devices';
            $data      = $unifidata->list_devices();
            break;
        case 'list_wlan_groups':
            $selection = 'list wlan groups';
            $data      = $unifidata->list_wlan_groups();
            break;
        case 'stat_sessions':
            $selection = 'stat sessions';
            $data      = $unifidata->stat_sessions();
            break;
        case 'list_users':
            $selection = 'list users';
            $data      = $unifidata->list_users();
            break;
        case 'list_rogueaps':
            $selection = 'list rogue access points';
            $data      = $unifidata->list_rogueaps();
            break;
        case 'list_events':
            $selection = 'list events';
            $data      = $unifidata->list_events();
            break;
        case 'list_alarms':
            $selection = 'list alarms';
            $data      = $unifidata->list_alarms();
            break;
        case 'count_alarms':
            $selection = 'count all alarms';
            $data      = $unifidata->count_alarms();
            break;
        case 'count_active_alarms':
            $selection = 'count active alarms';
            $data      = $unifidata->count_alarms(false);
            break;
        case 'list_wlanconf':
            $selection = 'list wlan config';
            $data      = $unifidata->list_wlanconf();
            break;
        case 'list_health':
            $selection = 'site health metrics';
            $data      = $unifidata->list_health();
            break;
        case 'list_dashboard':
            $selection = 'site dashboard metrics';
            $data      = $unifidata->list_dashboard();
            break;
        case 'list_settings':
            $selection = 'list site settings';
            $data      = $unifidata->list_settings();
            break;
        case 'list_sites':
            $selection = 'details of available sites';
            $data      = $sites;
            break;
        case 'list_extension':
            $selection = 'list VoIP extensions';
            $data      = $unifidata->list_extension();
            break;
        case 'list_portconf':
            $selection = 'list port configuration';
            $data      = $unifidata->list_portconf();
            break;
        case 'list_networkconf':
            $selection = 'list network configuration';
            $data      = $unifidata->list_networkconf();
            break;
        case 'list_dynamicdns':
            $selection = 'dynamic dns configuration';
            $data      = $unifidata->list_dynamicdns();
            break;
        case 'list_current_channels':
            $selection = 'current channels';
            $data      = $unifidata->list_current_channels();
            break;
        case 'list_portforwarding':
            $selection = 'list port forwarding rules';
            $data      = $unifidata->list_portforwarding();
            break;
        case 'list_portforward_stats':
            $selection = 'list port forwarding stats';
            $data      = $unifidata->list_portforward_stats();
            break;
        case 'list_dpi_stats':
            $selection = 'list DPI stats';
            $data      = $unifidata->list_dpi_stats();
            break;
        case 'stat_voucher':
            $selection = 'list hotspot vouchers';
            $data      = $unifidata->stat_voucher();
            break;
        case 'stat_payment':
            $selection = 'list hotspot payments';
            $data      = $unifidata->stat_payment();
            break;
        case 'list_hotspotop':
            $selection = 'list hotspot operators';
            $data      = $unifidata->list_hotspotop();
            break;
        case 'list_self':
            $selection = 'self';
            $data      = $unifidata->list_self();
            break;
        case 'stat_sites':
            $selection = 'all site stats';
            $data      = $unifidata->stat_sites();
            break;
        case 'list_admins':
            $selection = 'list_admins';
            $data      = $unifidata->list_admins();
            break;
        default:
            break;
    }
}

/**
 * count the number of objects collected from the UniFi controller
 */
if ($action != '') {
    $objects_count = count($data);
}

/**
 * construct the url for the Bootstrap CSS file based on the selected theme (standard Bootstrap or one of the Bootswatch themes)
 */
if ($theme === 'bootstrap') {
    $css_url = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css';
} else {
    $css_url = 'https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/' . trim($theme) . '/bootstrap.min.css';
}

/**
 * execute timing of data collection from UniFi controller
 */
$time_2          = microtime(true);
$time_after_load = $time_2 - $time_start;

/**
 * calculate all the timings/percentages
 */
$time_end    = microtime(true);
$time_total  = $time_end - $time_start;
$login_perc  = ($time_after_login/$time_total)*100;
$load_perc   = (($time_after_load - $time_after_login)/$time_total)*100;
$remain_perc = 100 - $login_perc-$load_perc;

/**
 * shared functions
 */
function print_output($output_format, $data)
{
    /**
     * function to print the output
     * switch depending on the selected $output_format
     */
    switch ($output_format) {
        case 'json':
            echo json_encode($data, JSON_PRETTY_PRINT);
            break;
        case 'json_color':
            echo json_encode($data);
            break;
        case 'php_array':
            print_r($data);
            break;
        case 'php_var_dump':
            var_dump($data);
            break;
        case 'php_var_export':
            var_export($data);
            break;
        default:
            echo json_encode($data, JSON_PRETTY_PRINT);
            break;
    }
}

/**
 * function to sort the sites collection alpabetically by description
 */
function sites_sort($site_a, $site_b)
{
    return strcmp($site_a->desc, $site_b->desc);
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Hostspot Management System - Dashboard</title>
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">

        <!-- build:css css/master.min.css -->
        <link rel="stylesheet" href="css/kube.css">
        <link rel="stylesheet" href="css/grafs.css">
        <link rel="stylesheet" href="css/master.css">
        <!-- endbuild -->
    </head>

    <body>
        <div class="loader"></div>
        <?php if (isset($_SESSION['controller'])) {?>

        <?php if (isset($unifidata)) { ?>

            <?php $listClients = $unifidata->list_clients() ;?>
            <?php $listDevices = $unifidata->list_devices() ;?>
            <section class="dash-top">

                <div class="row gutters align-middle">
                    <div class="col col-4">
                        <div class="top-title">
                            <img src="images/arrows.png" alt="">
                            <span>Hotspot Management System</span>
                        </div>

                        <div class="top-left-content">
                            <div class="row gutters auto">
                                <?php if(!empty($listClients)) {?>
                                    <?php
                                        $totalUsage = 0;
                                        foreach ($listClients as $key=>$value){
                                            if(isset($value->tx_bytes)){
                                                $totalUsage += $value->tx_bytes;
                                            }
                                        }  
                                    ?>
                                    <div class="col first">
                                        <p>Data usage for the last 12 Hours</p>
                                        <h1 class="data-usg"><?php echo number_format((float)$totalUsage / 1000000000, 2, '.', '') . 'GB'; ?></h1>
                                        <img src="images/kpi-value-up.png" alt="">
                                        <!--<div class="bottom-feed">
                                            <h4 class="data-down">10gb</h4>
                                            <img src="images/kpi-value-down.png" alt="">
                                        </div>-->
                                    </div>
                                <?php }?>
                                
        
                                <?php if(!empty($listClients)) { ?>
                                    <?php $clientNumber = count($listClients); ?>
                                    <div class="col second">
                                        <p>Active Clients</p>
                                        <h1 class="actv-dev"><?php echo $clientNumber; ?></h1>
                                        <img src="images/online-indicator.png" alt="">
                                    </div>
                                <?php } else { ?>
                                    <div class="col second">
                                        <p>Active Clients</p>
                                        <h1 class="actv-dev">0</h1>
                                        <img src="images/online-indicator.png" alt="">
                                    </div>
                                <?php }?>
                                
                                <?php if(!empty($listDevices)) { ?>
                                    <?php $deviceNumber = count($listDevices); ?>
                                    <div class="col">
                                        <p>Active APs</p>
                                        <h1 class="new-dev"><?php echo $deviceNumber; ?></h1>
                                        <img src="images/kpi-value-up.png" alt="">
                                    </div>
                                <?php } else { ?>
                                    <div class="col">
                                        <p>Active APs</p>
                                        <h1 class="new-dev">0</h1>
                                        <img src="images/kpi-value-up.png" alt="">
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col col-8">
                        <div class="graph-area">
                            <p class="graph-header">Data Usage for the last 12 hours</p>
                            <div id="usage-chart" style="max-width: 100%; height: 209px;"></div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="dash-body">
                <div class="row sub-content">
                    <div class="col col-2 side-menu">
                        <div class="body-logo">
                            <img src="images/access-logo.png" alt="Access Bank">
                        </div>

                        <div class="submenu">
                            <ul>
                                <li>
                                    <a href="">
                                        <span><img src="images/activities.png" alt=""></span>
                                        Activities
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <span><img src="images/users.png" alt=""></span>
                                        Users
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="devlist">
                            <p>Branches</p>
                            <?php if(empty($listDevices)) { ?>
                                <?php echo "<div class='message warning text-center upper' data-component='message'><p>" . "APs are disconnected at the moment." . "</p></div>"; ?> 
                            <?php } else { ?>
                            <ul class="sublist">
                                <li class="active"><a href="">All</a></li>
                                <?php foreach ($listDevices as $device) { ?>
                                    <?php echo "<li>" . "<a href='?action=list_ap_clients'" . "id='$device->mac'" . ">" . $device->name . "</a></li>" ;?>
                                <?php } ?>
                            </ul>
                            <?php } ?>
                           
                        </div>
                </div>

                <div class="col col-10 main-area">
                
                    <!-- Tab nav for devices filter -->
                    <nav class="tabs" data-component="tabs" data-equals="true">
                        <ul>
                            <li><a href="#actv_clients">Active Clients</a></li>
                            <li><a href="#off_clients">Offline Clients</a></li>
                            <li class="active"><a href="#all_clients">All</a></li>
                        </ul>
                    </nav>

                    <div class="data_container" id="actv_clients">
                        <table>
                            <thead>
                                <tr>
                                    <th class="w25">Device Name</th>
                                    <th class="w15">Device Type</th>
                                    <th class="w15">IP</th>
                                    <th class="w15">Status</th>
                                    <th class="w15">Data Usage</th>
                                    <th class="w15">Connection</th>
                                </tr>
                            </thead>

                            <tbody>

                            </tbody>
                        </table>
                    </div>

                    <div class="data_container" id="off_clients">
                        <table>
                            <thead>
                                <tr>
                                    <th class="w25">Device Name</th>
                                    <th class="w15">Device Type</th>
                                    <th class="w15">IP</th>
                                    <th class="w15">Status</th>
                                    <th class="w15">Data Usage</th>
                                    <th class="w15">Connection</th>
                                </tr>
                            </thead>

                            <tbody>
                            
                            </tbody>

                        </table>
                    </div>

                    <div class="data_container" id="all_clients">
                        <div id="client-collapse" class="table-content" data-component="collapse">
                            <div class="tHeader row auto">
                                <div class="col w20">Device Name</div>
                                <div class="col w15">Device Type</div>
                                <div class="col w15">IP</div>
                                <div class="col w10">Status</div>
                                <div class="col w20">Data Usage</div>
                                <div class="col w15">AP/Port</div>
                                <div class="col w5"></div>
                            </div>

                            <?php if(empty($listClients)) { ?>

                                <?php echo "<div class='message warning text-center upper' data-component='message'><p>" . "There are currently no clients connected to your APs." . "</p></div>"; ?>

                            <?php } else { ?>
                                <?php $boxNo = 0; ?>
                                <?php foreach ($listClients as $client) { ?>
                                    <?php
                                        $seconds = $client->uptime;
                                        $hours = floor($seconds / 3600);
                                        $seconds -= $hours * 3600;
                                        $minutes = floor($seconds / 60);
                                        $seconds -= $minutes * 60; 
                                    ?>
                                    <?php $boxNo++; ?>
                                    <?php $datarate = $client->tx_bytes; ?>
                                        <h4>
                                            <a href="<?php echo '#box-' . $boxNo; ?>" class="collapse-toggle">
                                                <div class="tContent row auto">
                                                    <?php echo "<div class='col w20 strong'>" . $client->hostname . "</div>"; ?>

                                                    <?php if(empty($client->oui)) { ?>
                                                        <?php echo "<div class='col w15'>" . 'N/A' . "</div>"; ?>
                                                    <?php } else { ?>
                                                        <?php echo "<div class='col w15'>" . $client->oui . "</div>"; ?>
                                                    <?php } ?>
                                                    
                                                    <?php if (empty($client->ip)) { ?>
                                                        <?php echo "<div class='col w15'>" . 'N/A' . "</div>"; ?>
                                                    <?php } else { ?>
                                                        <?php echo "<div class='col w15'>" . $client->ip . "</div>"; ?>
                                                    <?php } ?>
                                                    
                                                    <div class="col w10 status"><span><img src="images/online-indicator.png" alt=""></span>Active</div>

                                                    <?php if (empty($datarate)) { ?>
                                                        <?php echo "<div class='col w20'><strong>" . '0 B' . "</strong></div>"; ?>
                                                    <?php } ?>

                                                    <?php if ($datarate < 1000) { ?>
                                                        
                                                        <?php echo "<div class='col w20'><strong>" . $datarate . ' B' . "</strong>" . " in " . $hours . "h ". $minutes . "m " . $seconds . "s " . "</div>"; ?>
                                                    <?php } ?>

                                                    <?php if ($datarate > 1000 && $datarate < 1000000) { ?>
                                                        <?php echo "<div class='col w20'><strong>" . number_format((float)$datarate / 1000, 2, '.', '') . ' KB' . "</strong>" . " in " . $hours . "h ". $minutes . "m " . $seconds . "s " . "</div>"; ?>
                                                    <?php } ?>

                                                    <?php if ($datarate > 1000000 && $datarate < 1000000000) { ?>
                                                        <?php echo "<div class='col w20'><strong>" . number_format((float)$datarate / 1000000, 2, '.', '') . ' MB' . "</strong>" . " in " . $hours . "h ". $minutes . "m " . $seconds . "s " . "</div>"; ?>
                                                    <?php } ?>

                                                    <?php if ($datarate > 1000000000) { ?>
                                                        <?php echo "<div class='col w20'><strong>" . number_format((float)$datarate / 1000000000, 2, '.', '') . ' GB' . "</strong>" . " in " . $hours . "h ". $minutes . "m " . $seconds . "s " . "</div>"; ?>
                                                    <?php }?>

                                                    <?php $clientmac = $client->ap_mac; ?>
                                                    <?php foreach ($listDevices as $device) { ?>
                                                        <?php if($device->mac == $clientmac) { ?>
                                                            <?php echo "<div class='col w15'>". $device->name . "</div>"; ?>
                                                        <?php } ?>
                                                    <?php } ?>   
                                                    <div class="col w5 text-right"><span class="caret down"></span></div>
                                                </div>
                                            </a>
                                        </h4>
                                        <div class="collapse-box hide client-data" id="<?php echo 'box-' . $boxNo; ?>">
                                            <?php $totalUpload = $client->rx_bytes; ?>
                                            <?php $totalDownload = $client->tx_bytes; ?>
                                            <div class="tClientData row align-middle">
                                                <div class="col col-4">
                                                    <p class="textAllocated">Bandwidth Allocated</p>
                                                    <div class="row align-middle gutters">
                                                        <div class="col col-5 text-center">
                                                            <div class="allocField">
                                                                <?php if(empty($totalDownload)) { ?>
                                                                    <?php echo "<h1 class='dl-cap'>" . "0.0" . "<span><img src='images/download-icon.png' alt=''></span></h1>"; ?>
                                                                    <?php echo "<p>Mbps Download</p>"; ?>
                                                                <?php }?> 

                                                                <?php if ($totalDownload < 1000) { ?>
                                                                    <?php echo "<h1 class='dl-cap'>" . $totalDownload . "<span><img src='images/download-icon.png' alt=''></span></h1>"; ?>
                                                                    <?php echo "<p>Bps Download</p>"; ?>
                                                                <?php } ?>

                                                                <?php if ($totalDownload > 1000 && $totalDownload < 1000000) { ?>
                                                                    <?php echo "<h1 class='dl-cap'>" . number_format((float)$totalDownload / 1000, 1, '.', '') . "<span><img src='images/download-icon.png' alt=''></span></h1>"; ?>
                                                                    <?php echo "<p>Kbps Download</p>"; ?>
                                                                <?php } ?>

                                                                <?php if ($totalDownload > 1000000 && $totalDownload < 1000000000) { ?>
                                                                    <?php echo "<h1 class='dl-cap'>" . number_format((float)$totalDownload / 1000000, 1, '.', '') . "<span><img src='images/download-icon.png' alt=''></span></h1>"; ?>
                                                                    <?php echo "<p>Mbps Download</p>"; ?>
                                                                <?php } ?>

                                                                <?php if ($totalDownload > 1000000000) { ?>
                                                                    <?php echo "<h1 class='dl-cap'>" . number_format((float)$totalDownload / 1000000000, 1, '.', '') . "<span><img src='images/download-icon.png' alt=''></span></h1>"; ?>
                                                                    <?php echo "<p>Gbps Download</p>"; ?>
                                                                <?php } ?>
                                                                <!--<h1 class='dl-cap'>span><img src='images/download-icon.png' alt=''></span></h1>
                                                                <p>Mbps Download</p>-->
                                                            </div>

                                                        </div>

                                                        <div class="col col-2 text-center">
                                                            <div class="divwrapper">
                                                                <div class="vertDivider"></div>
                                                            </div>
                                                        </div>

                                                        <div class="col col-5 text-center">
                                                            <div class="allocField">
                                                                <?php if(empty($totalUpload)) { ?>
                                                                    <?php echo "<h1 class='ul-cap'>" . "0.0" . "<span><img src='images/upload-icon.png' alt=''></span></h1>"; ?>
                                                                    <?php echo "<p>Mbps Upload</p>"; ?>
                                                                <?php }?> 

                                                                <?php if ($totalUpload < 1000) { ?>
                                                                    <?php echo "<h1 class='ul-cap'>" . $totalUpload . "<span><img src='images/upload-icon.png' alt=''></span></h1>"; ?>
                                                                    <?php echo "<p>Bps Upload</p>"; ?>
                                                                <?php } ?>

                                                                <?php if ($totalUpload > 1000 && $totalUpload < 1000000) { ?>
                                                                    <?php echo "<h1 class='ul-cap'>" . number_format((float)$totalUpload / 1000, 1, '.', '') . "<span><img src='images/upload-icon.png' alt=''></span></h1>"; ?>
                                                                    <?php echo "<p>Kbps Upload</p>"; ?>
                                                                <?php } ?>

                                                                <?php if ($totalUpload > 1000000 && $totalUpload < 1000000000) { ?>
                                                                    <?php echo "<h1 class='ul-cap'>" . number_format((float)$totalUpload / 1000000, 1, '.', '') . "<span><img src='images/upload-icon.png' alt=''></span></h1>"; ?>
                                                                    <?php echo "<p>Mbps Upload</p>"; ?>
                                                                <?php } ?>

                                                                <?php if ($totalUpload > 1000000000) { ?>
                                                                    <?php echo "<h1 class='ul-cap'>" . number_format((float)$totalUpload / 1000000000, 1, '.', '') . "<span><img src='images/upload-icon.png' alt=''></span></h1>"; ?>
                                                                    <?php echo "<p>Gbps Upload</p>"; ?>
                                                                <?php } ?>
                                                                <!--<h1 class="ul-cap"><span><img src="images/upload-icon.png" alt=""></span></h1>
                                                                <p>Mbps Upload</p>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="text-right">
                                                        <button class="button setlimit outline" onclick="$.modalwindow({ target: '#client-details1', position: 'center', width: '989px' })">
                                                            Set Bandwidth Limit
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col col-6">
                                                    <p class="textAllocated">Data Allocated</p>
                                                    <div class="progressarea">
                                                        <progress max="2" value="1.8" class="dataprogress"></progress>
                                                        <div class="progress_desc">
                                                            <span class="used">1.8gb used</span>
                                                            <span class="fulld">2gb</span>
                                                        </div>
                                                    </div>

                                                    <div class="text-right">
                                                        <button class="button setlimit outline" onclick="$.modalwindow({ target: '#client-details1', position: 'center', width: '989px' })">
                                                            Set Data Limit
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col col-2 text-center">
                                                    <button class="button block">Block User</button>
                                                </div>
                                            </div>
                                        </div>

                                         <div id="client-details1" class="modal-box hide">
                                            <div class="modal">
                                                <span class="close"></span>
                                                <div class="modal-header text-center">Set Data Limit</div>
                                                <div class="modal-body">
                                                    <div class="row gutters">
                                                        <div class="col col-5">
                                                            <form action="" class="form" id="updateBandwidth">
                                                                <div class="row">
                                                                    <div class="col col-8 push-center">
                                                                        <p class="limit-header">Allocate by Bandwidth</p>
                                                                        <div class="form-item">
                                                                            <label for="">Select Bandwidth</label>
                                                                            <input type="text" class="mdl" placeholder="00 Mbps Download">
                                                                            <input type="text" class="mul" placeholder="00 Mbps Upload">
                                                                        </div>

                                                                        <div class="form-item">
                                                                            <button class="button update text-center w100" type="submit">
                                                                                Update
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>

                                                        <div class="col col-2 text-center">
                                                            <div class="wrapper">
                                                                <div class="line"></div>
                                                                <div class="wordwrapper">
                                                                    <div class="word">or</div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col col-5">
                                                            <form action="" id="updateData" class="form">
                                                                <div class="row">
                                                                    <div class="col col-8 push-center">
                                                                        <p class="limit-header">Allocate by Data Size</p>
                                                                        <div class="form-item">
                                                                            <label for="">Select Data</label>
                                                                            <input type="text" class="" placeholder="00 MB">
                                                                        </div>

                                                                        <div class="form-item">
                                                                            <button class="button update text-center w100" type="submit">
                                                                                Update
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!--<div id="client-details2" class="modal-box hide">
                                            <div class="modal">
                                                <span class="close"></span>
                                                <div class="modal-header text-center">Set Data Limit</div>
                                                <div class="modal-body">
                                                    <div class="row gutters">
                                                        <div class="col col-5">
                                                            <form action="" id="updateData" class="form">
                                                                <div class="row">
                                                                    <div class="col col-8 push-center">
                                                                        <p class="limit-header">Allocate by Data Size</p>
                                                                        <div class="form-item">
                                                                            <label for="">Select Data</label>
                                                                            <input type="text" class="" placeholder="00 MB">
                                                                        </div>

                                                                        <div class="form-item">
                                                                            <button class="button update text-center w100" type="submit">
                                                                                Update
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>

                                                        <div class="col col-2 text-center">
                                                            <div class="wrapper">
                                                                <div class="line"></div>
                                                                <div class="wordwrapper">
                                                                    <div class="word">or</div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col col-5">
                                                            <form action="" class="form" id="updateBandwidth">
                                                                <div class="row">
                                                                    <div class="col col-8 push-center">
                                                                        <p class="limit-header">Allocate by Bandwidth</p>
                                                                        <div class="form-item">
                                                                            <label for="">Select Bandwidth</label>
                                                                            <input type="text" class="mdl" placeholder="00 Mbps Download">
                                                                            <input type="text" class="mul" placeholder="00 Mbps Upload">
                                                                        </div>

                                                                        <div class="form-item">
                                                                            <button class="button update text-center w100" type="submit">
                                                                                Update
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                <?php } ?>
                            <?php } ?>
                        </div>                  
                    </div>
                </div>
            </section>

            

        <!-- build:js js/dashboard.min.js -->
        <script src="js/third-party/jquery.min.js"></script>
        <script src="js/third-party/jquery.validate.min.js"></script>
        <script src="js/third-party/grafs.min.js"></script>
        <script src="js/third-party/kube.min.js"></script>
        <script src="js/third-party/jquery.simplePagination.js"></script>
        <script src="js/config.js"></script>
        <script src="js/dashboard.js"></script>
        <script>
            var clientList = <?php echo json_encode($listClients); ?>;
            var deviceList = <?php echo json_encode($listDevices); ?>;

            $('#68:72:51:68:14:6f').on('click', function(e){
                e.preventDefault();
                console.log($(this));
            })
            $('#68:72:51:68:14:70').on('click', function(e){
                e.preventDefault();
                console.log($(this));
            })
            $('#04:18:d6:62:9f:85').on('click', function(e){
                e.preventDefault();
                console.log($(this));
            })
            
        </script>
        <?php } ?>
        <?php } ?>
    </body>
</html>